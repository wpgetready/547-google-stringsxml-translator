/*20190117: Google Strings.xml Translator
 * What would be needed in order to make a useful app capable of translating strings.xml?
 * 1-Providing where is res folder for the app
 * 2-The class responsible of saving data needs also to make folder if needed.
 * 3-It also responsible of creating proper folden names based on local code.
 * 4 - According to
 * https://cloud.google.com/translate/faq
 * it is possible to indicate something to DO NOT translate.
 * However, in the tests I confirmed that and it is impractical, adding unnecesary tags
 * which are not deleted in the process.
 * 20190404: Many improvements. Mainly, now I'm using Unit Testig for complex evaluations, which simplified testing a lot.
 ** */

import java.io.*;
import java.nio.channels.FileChannel;
import javax.swing.SwingUtilities;
import javax.swing.JComponent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;
import javax.swing.JFrame;
import javax.swing.BorderFactory;
import javax.swing.text.DefaultCaret;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JScrollPane;
import java.awt.GridLayout;
import javax.swing.JList;
import javax.swing.JOptionPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.Box;
import java.awt.Dimension;
import java.nio.file.NotDirectoryException;
import java.util.Arrays;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Main extends JPanel {
    private static final long serialVersionUID = 1L;
    private static String sCfgFile;
    JLabel infoLabel;
    JComboBox sectionList;
    JCheckBox chkCL;
    private boolean shouldCapitalize;
    private boolean shouldNotTranslateTitle;
    private boolean shouldNotTranslateShort;
    private boolean shouldNotTranslateLong;
    private int nSelectedIdx;
    private String sSelectedName;
    private JTextArea textAreaLog;
    private JTextField jTitle;
    private JTextField jtf;
    private JTextField jShortDesc;
    private JTextArea jLongDesc;
    private JButton btnTrans;
    private JButton btnTransAll;
    private JButton btnUploadTranslations;
    private JButton btnSave;
    private JButton btnAddLang;
    private JButton btnDelLang;
    private JButton btnClearLog;
    private JButton btnValidate;

    private JButton btnSaveOneStringsXML;
    private JButton btnSaveStringsXML;

    boolean testActionListenerActive;
    private JCheckBox chkDoNotTitle;
    private JCheckBox chkDoNotShort;
    private JCheckBox chkDoNotLong;
    private DataPack data;

    private void initCombo() {
        this.nSelectedIdx = 0;
        this.sectionList.setSelectedIndex(this.nSelectedIdx);
        this.sSelectedName = (String) this.sectionList.getSelectedItem();
    }

    public Main() {
        this.shouldCapitalize = true;
        this.shouldNotTranslateTitle = false;
        this.shouldNotTranslateShort = false;
        this.shouldNotTranslateLong = false;
        this.testActionListenerActive = true;
        this.setLayout(new BoxLayout(this, 1));
        this.loadConfigs();
        final JLabel lbl0 = new JLabel("<html>INSTRUCTIONS: this app will translate strings.xml. Google Translator (GT) has FLAWS , so " +
                " use this 3-steps strategy: <br/><br/>" +
                "STEP 1 -Copy strings.xml in English in the form,unaltered . MAKE SURE everything is correctly writing (not gramatical errors).<br/>" +
                "STEP 2 - Call Translator for your favorite language and check if translation works. <br/>" +
                "STEP 3 - If everything is ok, translater for all languages and SAVE THE RESULT USING 'Save  .gpup' before touch anything!<br/>" +
                "(You have a chance of correcting things BEFORE saving to strings.xml. IMPORTANT: saving to strings.xml will fix many things. KEEP READING!<br/>" +
                "4 - This app will fix MANY issues WHEN SAVING to strings.xml (including: correct aphostrofe (') issue, trim spaces within html tags, restore carriage return and parameters $1s,$2s, etc. <br/>" +
                "5 - NOTE 1: GT DESTROYS indenting: this program reconstruct indenting after translation. Otherwise, results would be unbearable!<br/>" +
                "6 - NOTE 2 (NEW!): There are tags <#dnt>...</#dnt> or <#do_not_translate>...</#do_not_translate> to avoid certains lines for being translated <br/>" +
                "IMPORTANT: Tags for not translation has to be in separated lines without any other characters or tags<br/>" +
                "7 - NOTE 3: GT IGNORES in some cases the uppercase/lower case. Example: Translating month's names sometimes are returned in uppercase first letter, sometime lowercase.<br/>" +
                "8 - NOTE 4: GT DESTROYS special caracters like '\\ n'. This is CHANGE PER LANGUAGE (example:it will happen in Spanish, but not in French(!)). It's up to you to put special chars where they suppose to be!!!<br/>" +
                "9 - NOTE 5: GT DESTROYS nested tags leaving only the translation.<br/>" +
                "Example: if you have a nested &#60;a>&#60;b>Hello&#60;b/>&#60;b>Dear&#60;b/>&#60;/a> the translator will rturn &#60;a> Hola Querida &#60;/a>, stripping the &#60;b> tag!<br/>" +
                "There is not a good workaround for this issue, except pay close attention and fix it.</br>" +
                "Being said that, it's up to you workaround ALL these issues. Check if all strings.xml are perfectly corrected.GOOD LUCK!!!<br/>");
        this.add(lbl0);

        /*
        final JLabel lbl1 = new JLabel("PROJECT PACKAGENAME : " + this.data.getPackageName(), 2);
        this.add(lbl1);
*/

        final JLabel lbl1xq = new JLabel("Generate Resource Folder (strings.xml) at: " + this.data.getAndroidResourcePath() + "\n", 2);
        this.add(Box.createRigidArea(new Dimension(5, 15)));
        this.add(lbl1xq);


        this.add(Box.createRigidArea(new Dimension(5, 30)));
        final JLabel lbl2 = new JLabel("Source dir : " + this.data.getSourceProjectDirectoryName(), 2);
        lbl2.setFont(lbl2.getFont().deriveFont(2));
        this.add(lbl2);

        lbl2.setAlignmentX(0.0f);
        this.add(Box.createRigidArea(new Dimension(5, 15)));
        final JLabel lbl3 = new JLabel("Language :", 0);
        this.sectionList = new JComboBox(this.data.getLangComboNames());


        this.initCombo();
        this.sectionList.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent e) {
                if (Main.this.testActionListenerActive) {
                    Main.this.saveSelectedLanguage();
                    final JComboBox cb = (JComboBox) e.getSource();
                    Main.access$1(Main.this, (String) cb.getSelectedItem());
                    Main.access$2(Main.this, cb.getSelectedIndex());
                    Main.this.loadSelectedLanguage();
                }
            }
        });
        final Box bCombo = Box.createHorizontalBox();
        bCombo.setAlignmentX(0.0f);
        bCombo.add(lbl3);
        bCombo.add(Box.createRigidArea(new Dimension(5, 5)));
        bCombo.add(this.sectionList);
        bCombo.add(Box.createRigidArea(new Dimension(5, 5)));
        (this.btnAddLang = new JButton("Add")).setToolTipText("add more languages.");
        this.btnAddLang.setPreferredSize(new Dimension(65, 30));
        this.btnAddLang.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent ae) {
                final String[] choices = Main.this.data.getNotPresentLangComboNames();
                if (choices.length == 0) {
                    JOptionPane.showMessageDialog(null, "I'm sorry, you already have all possible languages!");
                    return;
                }
                final JList list = new JList(choices);


                final JPanel panel = new JPanel(new GridLayout(1, 2));
                final JScrollPane jscr = new JScrollPane(list);
                jscr.setPreferredSize(new Dimension(150, 510));
                panel.add(jscr);
                final int x = JOptionPane.showConfirmDialog(null, panel, "Add new language(s)", 2);
                final int[] selix = list.getSelectedIndices();
                if (x == 0) {
                    Main.this.saveSelectedLanguage();
                    for (int i = 0; i < selix.length; ++i) {
                        final String s = choices[selix[i]];
                        Main.this.data.addLangByName(s);
                    }
                    Main.this.testActionListenerActive = false;
                    Main.this.sectionList.setModel(new DefaultComboBoxModel<String>(Main.this.data.getLangComboNames()));
                    Main.this.initCombo();
                    Main.this.loadSelectedLanguage();
                    Main.this.testActionListenerActive = true;
                }
            }
        });
        bCombo.add(this.btnAddLang);
        (this.btnDelLang = new JButton("Del")).setToolTipText("remove currently selected language.");
        this.btnDelLang.setPreferredSize(new Dimension(65, 30));
        this.btnDelLang.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent ae) {
                if (Main.this.nSelectedIdx > 0) {
                    final int dialogButton = 0;
                    final int dialogResult = JOptionPane.showConfirmDialog(null, "Do you really want to remove this language?", "Question", dialogButton);
                    if (dialogResult == 0) {
                        Main.this.testActionListenerActive = false;
                        Main.this.saveSelectedLanguage();
                        Main.this.data.delLang(Main.this.nSelectedIdx);
                        Main.access$2(Main.this, -1);
                        Main.this.sectionList.setModel(new DefaultComboBoxModel<String>(Main.this.data.getLangComboNames()));
                        Main.this.initCombo();
                        Main.this.loadSelectedLanguage();
                        Main.this.testActionListenerActive = true;
                    }
                }
            }
        });
        bCombo.add(this.btnDelLang);
        this.add(bCombo);
/*
        final JLabel lblTitle = new JLabel("Title :", 0);
        this.jTitle = new JTextField(60);
        (this.chkCL = new JCheckBox("Capitalize first letters", true)).addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(final ItemEvent e) {
                Main.access$7(Main.this, e.getStateChange() == 1);
            }
        });
        */
        /*
        final Box boxTitle = Box.createHorizontalBox();
        boxTitle.setAlignmentX(0.0f);
        boxTitle.add(lblTitle);
        boxTitle.add(Box.createRigidArea(new Dimension(5, 25)));
        boxTitle.add(this.jTitle);
        boxTitle.add(this.chkCL);
        this.add(boxTitle);
        this.chkDoNotTitle = new JCheckBox("Skip title", false);
        this.chkDoNotShort = new JCheckBox("Skip short desc", false);
        this.chkDoNotLong = new JCheckBox("Skip long desc", false);
        final Box boxExclusions = Box.createHorizontalBox();
        boxExclusions.setAlignmentX(0.0f);
        boxExclusions.add(this.chkDoNotTitle);
        boxExclusions.add(this.chkDoNotShort);
        boxExclusions.add(this.chkDoNotLong);
        this.chkDoNotTitle.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(final ItemEvent e) {
                Main.access$8(Main.this, e.getStateChange() == 1);
            }
        });
        this.chkDoNotShort.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(final ItemEvent e) {
                Main.access$9(Main.this, e.getStateChange() == 1);
            }
        });
        this.chkDoNotLong.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(final ItemEvent e) {
                Main.access$10(Main.this, e.getStateChange() == 1);
            }
        });
        this.add(boxExclusions);
        */

        /*
        final JLabel lbl3C = new JLabel("Short description :", 0);
        this.jShortDesc = new JTextField(30);
        final Box bCombo2 = Box.createHorizontalBox();
        bCombo2.setAlignmentX(0.0f);
        bCombo2.add(lbl3C);
        bCombo2.add(Box.createRigidArea(new Dimension(5, 5)));
        bCombo2.add(this.jShortDesc);
        this.add(bCombo2);
        final JLabel lbl4 = new JLabel("Long description :", 2);
        lbl4.setAlignmentX(0.0f);
        this.add(lbl4);
        */
        this.jLongDesc = new JTextArea(18, 80);
        final JScrollPane scrollPane = new JScrollPane(this.jLongDesc);
        scrollPane.setVerticalScrollBarPolicy(22);
        scrollPane.setAlignmentX(0.0f);
        this.jLongDesc.setEditable(true);
        this.jLongDesc.setLineWrap(true);
        this.add(scrollPane);
        final JLabel lbl5 = new JLabel("Log :", 2);
        lbl5.setAlignmentX(0.0f);
        this.add(lbl5);
        this.textAreaLog = new JTextArea(15, 80);
        final DefaultCaret caret = (DefaultCaret) this.textAreaLog.getCaret();
        caret.setUpdatePolicy(2);
        final JScrollPane scrollPaneLog = new JScrollPane(this.textAreaLog);
        scrollPaneLog.setVerticalScrollBarPolicy(22);
        scrollPaneLog.setAlignmentX(0.0f);
        this.textAreaLog.setEditable(false);
        this.add(scrollPaneLog);
        this.add(Box.createRigidArea(new Dimension(5, 15)));
        (this.btnTrans = new JButton("Translate Selected")).setPreferredSize(new Dimension(177, 30));
        this.btnTrans.setToolTipText("call Google Translate and translate currently selected language only.");
        this.btnTrans.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent ae) {
                Main.this.doStartTranslate(false);
            }
        });
        (this.btnTransAll = new JButton("Translate All")).setPreferredSize(new Dimension(177, 30));
        this.btnTransAll.setToolTipText("call Google Translate and translate all languages added to current project (via Add button).");
        this.btnTransAll.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent ae) {
                Main.this.doStartTranslate(true);
            }
        });
        (this.btnSave = new JButton("Save .gpup")).setPreferredSize(new Dimension(177, 30));
        this.btnSave.setToolTipText("save current translation results to your .gpup file.");
        this.btnSave.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent ae) {
                Main.this.saveSelectedLanguage();
                if (Main.this.data.saveToFile(Main.sCfgFile) != 0) {
                    JOptionPane.showMessageDialog(null, "Error saving file.", "Error", -1, null);
                }
            }
        });
/*
        (this.btnUploadTranslations = new JButton("Upload Translations to Google Play")).setPreferredSize(new Dimension(177, 30));
        this.btnUploadTranslations.setEnabled(false); //disable button , it will be enabled only after validation
        this.btnUploadTranslations.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(final ActionEvent ae) {
                Main.this.doStartUpload();
            }
        });

        this.btnUploadTranslations.setToolTipText("upload all your translations to Google Play dev console using GOOGLE API");
*/

        (this.btnClearLog = new JButton("Clear Log")).setPreferredSize(new Dimension(177, 30));
        //btnCreate(this.btnClearLog,"Clear Log",177,30,"Clear text logs");
        this.btnClearLog.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (textAreaLog != null) {
                    //https://stackoverflow.com/questions/15798532/how-to-clear-jtextarea
                    //Quite absurd...
                    textAreaLog.selectAll();
                    textAreaLog.replaceSelection("");
                }
            }
        });

        /*
        (this.btnValidate=new JButton("Validate all Data")).setPreferredSize(new Dimension(177,30));
        this.btnValidate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                validateUpload(true);
            }
        });
        */

/***/
        (this.btnSaveStringsXML = new JButton("Save to strings.xml")).setPreferredSize(new Dimension(177, 30));

        this.btnSaveStringsXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String ARP = data.getAndroidResourcePath().trim();
                if (ARP != "") {
                    try {
                        DeployTranslation.saveStringsXML(ARP, data.arrTrans, Main.this);
                    } catch (InterruptedException e1) {
                        Main.this.addLog("Interrupted exception error ");
                    } catch (NotDirectoryException ex) {
                        Main.this.addLog("Unexpected Error: " + ex.getMessage());
                    }
                } else {
                    Main.this.addLog("ERROR: AndroidResourcePath empty, check file .gpup");
                }

            }
        });

        (this.btnSaveOneStringsXML = new JButton("Save this strings.xml")).setPreferredSize(new Dimension(177, 30));

        this.btnSaveOneStringsXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String ARP = data.getAndroidResourcePath().trim();
                if (ARP != "") {
                    String result = null;
                    result = DeployTranslation.saveOneStringsXML(ARP, data.arrTrans.get(nSelectedIdx), nSelectedIdx, Main.this);
                    addLog("Translation saved on folder " + result);
                } else {
                    addLog("ERROR: AndroidResourcePath empty, check file .gpup");
                }
            }
        });

/***/
        final Box boxBtns = Box.createHorizontalBox();
        boxBtns.setAlignmentX(0.0f);
        boxBtns.add(this.btnTrans);
        boxBtns.add(Box.createRigidArea(new Dimension(5, 5)));

        boxBtns.add(this.btnTransAll);

        boxBtns.add(Box.createRigidArea(new Dimension(5, 5)));
        boxBtns.add((this.btnSaveStringsXML));

        boxBtns.add(Box.createRigidArea(new Dimension(5, 5)));
        boxBtns.add((this.btnSaveOneStringsXML));

        boxBtns.add(Box.createRigidArea(new Dimension(15, 5)));
        boxBtns.add(this.btnSave);
        boxBtns.add(Box.createRigidArea(new Dimension(5, 5)));
        //    boxBtns.add(this.btnValidate);
        boxBtns.add(Box.createRigidArea(new Dimension(5, 5)));
//        boxBtns.add(this.btnUploadTranslations);
        boxBtns.add(Box.createRigidArea(new Dimension(5, 5)));
        boxBtns.add((this.btnClearLog));


        this.add(boxBtns);
        this.loadSelectedLanguage();
        this.setBorder(BorderFactory.createEmptyBorder(12, 12, 12, 24));

    }

    private void validateUpload(boolean showMsg) {
        //Save current language in case you made changes
        //(Note: changing language automatically saves changes also)
        saveSelectedLanguage();
        final String s = data.validation();
        if (!s.isEmpty()) {
            if (showMsg) {
                Main.this.addLog("WARNING! Check following issues first.(Upload aborted).");
                Main.this.addLog(s);
            }
            //btnUploadTranslations.setEnabled(false);
        } else {
            if (showMsg) {
                Main.this.addLog("VALIDATION PASS: Ready for starting upload.");
            }
            //btnUploadTranslations.setEnabled(true);
        }
    }

    //Experimental, failed (why?)
    private void btnCreate(JButton btn, String txt, int width, int height, String tooltip) {
        (btn = new JButton(txt)).setPreferredSize(new Dimension(width, height));
        btn.setToolTipText(tooltip);
    }

    private void saveSelectedLanguage() {
        if (this.nSelectedIdx >= 0) {
            //this.data.setLang_Title(this.nSelectedIdx, this.jTitle.getText());
//            this.data.setLang_ShortDesc(this.nSelectedIdx, this.jShortDesc.getText());
            this.data.setLang_LongDesc(this.nSelectedIdx, this.jLongDesc.getText());
        }
    }

    private void loadSelectedLanguage() {
//        this.jTitle.setText(this.data.getLang_Title(this.nSelectedIdx));
//        this.jShortDesc.setText(this.data.getLang_ShortDesc(this.nSelectedIdx));
        this.jLongDesc.setText(this.data.getLang_LongDesc(this.nSelectedIdx));
    }

    private static void createAndShowGUI() {
        final JFrame frame = new JFrame("Main");
        frame.setDefaultCloseOperation(3);
        frame.setTitle("Android Studio strings.xml Translator version 1.0.3");
        final JComponent newContentPane = new Main();
        newContentPane.setOpaque(true);
        frame.setContentPane(newContentPane);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent e) {
                Main.sCfgFile.length();
                e.getWindow().dispose();
            }
        });
        frame.pack();
        frame.setVisible(true);
    }

    public DataPack getData() {
        return this.data;
    }

    private void loadConfigs() {
        (this.data = new DataPack()).loadFromFile(Main.sCfgFile);
    }

    public void addLog(final String s, Boolean cr) {
        if (cr) {
            this.textAreaLog.append(s + "\n");
        } else {
            this.textAreaLog.append(s);
        }
        this.textAreaLog.setCaretPosition(this.textAreaLog.getDocument().getLength());

    }

    public void addLog(final String s) {
        addLog(s,true);
    }

    private void disableAllWidgets() {
        this.sectionList.setEnabled(false);
        //this.btnUploadTranslations.setEnabled(false);
        this.btnTrans.setEnabled(false);
        this.btnTransAll.setEnabled(false);
        this.btnSave.setEnabled(false);
        this.btnAddLang.setEnabled(false);
        this.btnDelLang.setEnabled(false);
        //this.jTitle.setEnabled(false);
        //this.jShortDesc.setEnabled(false);
        this.jLongDesc.setEnabled(false);
    }

    private void enableAllWidgets() {
        this.sectionList.setEnabled(true);
        validateUpload(false); //Validate and do not Show button if data needs to be improved
        this.btnTrans.setEnabled(true);
        this.btnTransAll.setEnabled(true);
        this.btnSave.setEnabled(true);
        this.btnAddLang.setEnabled(true);
        this.btnDelLang.setEnabled(true);
        //this.jTitle.setEnabled(true);
        //this.jShortDesc.setEnabled(true);
        this.jLongDesc.setEnabled(true);
    }

    //New translation is simplified, since we do not need any title or description
    public void doStartTranslate(final boolean all) {
        final DataPack data = this.data;
        this.saveSelectedLanguage();
        this.disableAllWidgets();
        new Thread() {
            @Override
            public void run() {
                try {
                    //20190405:It wasn't clear in the first versions, but you can't translate the default
                    //This is a msg to make sure app does do nothing when nothing has to do...
                    if (!all && Main.this.nSelectedIdx == 0) {
                        addLog("I can't translate DEFAULT LANGUAGE!!!. Aborting...");
                        Main.this.loadSelectedLanguage();
                        Main.this.enableAllWidgets();
                        return;
                    }
                    final GTrans2 gt = new GTrans2();
                    gt.initTranslation();
                    Main.this.addLog("Starting automatic translation...");
                    final String sBaseLongDesc = data.getLang_LongDesc(0);
                    String[] out = null;
                    for (int i = 1; i < data.getLang_ArrCount(); ++i) {
                        if (all || i == Main.this.nSelectedIdx) {
                            final String destLangCode = data.getLang_GTCode(i);
                            boolean copyContents = false;
                            //Important: this loops does this important thing:
                            //If we found a language with same language code (es-US es-BR,or es-ES es-US)
                            //We won't translate the language but copy it.
                            //20190405: In case of english, also strip special tags.
                            for (int j = 0; j < i && !copyContents; ++j) {
                                if (data.getLang_GTCode(j) == destLangCode) {
                                    data.setLang_LongDesc(i, GTrans2.stripDNT_Tags(data.getLang_LongDesc(j)));
                                    copyContents = true;
                                    Main.this.addLog("Translation copied " + data.getLang_Code(j) + " => " + data.getLang_Code(i));
                                }
                            }
                            if (copyContents) continue;//language copied, proceed next language

                            out = gt.doTranslateV2(sBaseLongDesc, destLangCode);
                            if (out == null) {
                                Main.this.addLog("Aborted translation due an error");
                                return;
                            }
                            //Google does not pretiffy the doc, make it so.
                            String prettyXML = DeployTranslation.prettifyDoc(String.join("\n", out));  //Pretiffy XML
                            data.setLang_LongDesc(i, prettyXML);
                            Main.this.addLog("Translated to " + data.getLang_Code(i));

                            Main.this.addLog("Cooldown 5 seconds before restart...");
                            Thread.sleep(5000); //We cannot saturate Google API Translate (we can ending up with an error)
                        }
                    }
                    Main.this.addLog("Translation finished.");
                } catch (Exception e) {
                    final StringWriter sw = new StringWriter();
                    final PrintWriter pw = new PrintWriter(sw);
                    e.printStackTrace(pw);
                    Main.this.addLog(sw.toString());
                }
                SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        Main.this.loadSelectedLanguage();
                        Main.this.enableAllWidgets();
                    }
                });
            }
        }.start();
    }

    public void doStartUpload() {
        final DataPack data = this.data;
        this.saveSelectedLanguage();
        this.disableAllWidgets();
        new Thread() {
            @Override
            public void run() {
                try {
                    final String s = data.validation();
                    if (!s.isEmpty()) {
                        Main.this.addLog("WARNING! Check following issues first.(Upload aborted)");
                        Main.this.addLog(s);
                    } else {
                        Main.this.addLog("Starting upload...");
                        final BasicUploadApk u = new BasicUploadApk(data, Main.this.shouldCapitalize);
                        u.doStart(Main.this);
                    }
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            Main.this.enableAllWidgets();
                        }
                    });
                    Main.this.addLog("Finished.");
                } catch (Exception e) {
                    final StringWriter sw = new StringWriter();
                    final PrintWriter pw = new PrintWriter(sw);
                    e.printStackTrace(pw);
                    Main.this.addLog(sw.toString());
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            Main.this.enableAllWidgets();
                        }
                    });
                }
            }
        }.start();
    }

    public void doRestore() {
    }

    public static void copyFile(final File sourceFile, final File destFile) throws IOException {
        if (!destFile.exists()) {
            destFile.createNewFile();
        }
        FileChannel source = null;
        FileChannel destination = null;
        try {
            source = new FileInputStream(sourceFile).getChannel();
            destination = new FileOutputStream(destFile).getChannel();
            destination.transferFrom(source, 0L, source.size());
        } finally {
            if (source != null) {
                source.close();
            }
            if (destination != null) {
                destination.close();
            }
        }
        if (source != null) {
            source.close();
        }
        if (destination != null) {
            destination.close();
        }
    }

    public String capitalize(final String s1) {
        if (s1.length() == 0) {
            return "";
        }
        final String s2 = String.valueOf(s1.substring(0, 1).toUpperCase()) + s1.substring(1);
        return s2;
    }

    public String capitalizeTitle(final String s1) {
        if (s1.length() == 0) {
            return "";
        }
        String s2 = s1.substring(0, 1).toUpperCase();
        for (int ix = 1; ix < s1.length(); ++ix) {
            if (s1.charAt(ix - 1) == ' ') {
                s2 = String.valueOf(s2) + s1.substring(ix, ix + 1).toUpperCase();
            } else {
                s2 = String.valueOf(s2) + s1.substring(ix, ix + 1);
            }
        }
        return s2;
    }

    public static void main(final String[] args) {
        if (i13() == 0) {
            System.exit(-1);
        }
        if (args.length == 0) {
            Main.sCfgFile = "default.gpup";
        } else {
            Main.sCfgFile = args[0];
        }
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                createAndShowGUI();
            }
        });
    }


    public static int i13() {
        return 1;
    }

    static /* synthetic */ void access$1(final Main main, final String sSelectedName) {
        main.sSelectedName = sSelectedName;
    }

    static /* synthetic */ void access$2(final Main main, final int nSelectedIdx) {
        main.nSelectedIdx = nSelectedIdx;
    }

    static /* synthetic */ void access$7(final Main main, final boolean shouldCapitalize) {
        main.shouldCapitalize = shouldCapitalize;
    }

    static /* synthetic */ void access$8(final Main main, final boolean shouldNotTranslateTitle) {
        main.shouldNotTranslateTitle = shouldNotTranslateTitle;
    }

    static /* synthetic */ void access$9(final Main main, final boolean shouldNotTranslateShort) {
        main.shouldNotTranslateShort = shouldNotTranslateShort;
    }

    static /* synthetic */ void access$10(final Main main, final boolean shouldNotTranslateLong) {
        main.shouldNotTranslateLong = shouldNotTranslateLong;
    }
}
