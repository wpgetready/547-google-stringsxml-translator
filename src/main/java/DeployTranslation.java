import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.nio.file.NotDirectoryException;
import java.util.Vector;

//This class is responsible of saving code to every other strings.xml
//More information:
//https://developer.android.com/guide/topics/resources/localization
public class DeployTranslation {
    public static String FILENAME = "/strings.xml";

    /**
     * Saves all data in several strings.xml resources.
     * Issue:
     *
     * @param initialFolder
     * @param arrTrans
     */
    public static void saveStringsXML(String initialFolder,
                                      Vector<DataPack.TransData> arrTrans,
                                      Main callback) throws InterruptedException, NotDirectoryException {

        String folderName = "";
        //Iterate through all data

        for (int x = 0; x < arrTrans.size(); ++x) {
            DataPack.TransData td = arrTrans.get(x);
            folderName=saveOneStringsXML(initialFolder, td, x, callback);
            callback.addLog("Saving... " + folderName );
        }
    }

    /**
     * Save strings.xml in the proper folder.
     * Returns complete path where the file was saved, useful for feedback purposes.-
     *
     * @param initialFolder
     * @param td
     * @param x
     * @return
     */
    public static String saveOneStringsXML(String initialFolder, DataPack.TransData td, int x, Main callback) {
        String folderName;
        File currentFolder;

        folderName = generateFolderName(initialFolder, x, td);  //Si el folder no existe, crearlo.
        currentFolder = new File(folderName);
        if (!currentFolder.isDirectory()) {
            Boolean result = currentFolder.mkdir();
        }
        //Finalmente guardar un archivo con el contenido esperado.
        //Atencion: solo usar LongDescription.
        try {
            return fileSave(currentFolder, td);
        } catch (FileNotFoundException e) {
            callback.addLog("FileNotFoundException: " + e.getMessage());
            return "";
        } catch (UnsupportedEncodingException e) {
            callback.addLog("UnsupportedEncodingException: " + e.getMessage());
            return "";
        } catch (SAXException e) {
            callback.addLog("SAXException: " + e.getMessage());
        } catch (ParserConfigurationException e) {
            callback.addLog("ParserConfigurationException: " + e.getMessage());
            return "";
        } catch (IOException e) {
            callback.addLog("IOException: " + e.getMessage());
            return "";
        }
        return "";
    }

    /**
     * given a directory, saves the data into strings.xml.
     *
     * @param directory
     * @param td
     */
    private static String fileSave(File directory, DataPack.TransData td) throws IOException, SAXException, ParserConfigurationException {
        String fPath = directory.getPath() + "\\strings.xml";
        if (td.sLongDesc.trim().equals("")){
            throw new IOException("ERROR: Description is empty or is not an XML File! Check " + td.sLanguageCode + " version");
        }
        String xmlFixed = DeployTranslation.transformXML(td.sLongDesc); //Do the best to recompose the document.

        final PrintWriter writer = new PrintWriter(fPath, "UTF-8");
        writer.println(xmlFixed);
        writer.close();
        return fPath;
    }

    //20190405:Strips special tags before saving to strings.xml
    public static String transformXML(String XMLDocument) throws ParserConfigurationException, SAXException, IOException {
        String strippedDoc = GTrans2.stripDNT_Tags(XMLDocument);
        Document doc = processXML(strippedDoc); //process doc recursively and suppress empty spaces to both sides of every node
        return toPrettyString(doc, 4); //indent XML document.
    }

    public static String prettifyDoc(String XMLDocument) throws IOException, SAXException, ParserConfigurationException {
        Document doc = getDocument(XMLDocument);
        return toPrettyString(doc, 4);
    }

    /**
     * We have few issues with document when translating
     * 1-The document has no format
     * 2-When translated, every item will have space both sides, left and right.
     * So, we need to process every node and then formatting it.
     *
     * @param XmlDoc
     * @return
     */
    private static Document processXML(String XmlDoc) throws IOException, SAXException, ParserConfigurationException {
        Document document = getDocument(XmlDoc);
        nodeIteration(document.getDocumentElement());
        return document;
    }

    private static Document getDocument(String XmlDoc) throws ParserConfigurationException, IOException, SAXException {
        Document document = null;
        document = DocumentBuilderFactory.newInstance()
                .newDocumentBuilder()
                .parse(new InputSource(new ByteArrayInputStream(XmlDoc.getBytes("utf-8"))));
        return document;
    }

    /**
     * Recursively process each node in document an trim it.
     * This method is where any filter needs to be added (clear spaces replace string(s), etc.)
     * Tasks to be completed:
     * -trim data
     * -replace '  by \' (tip: use \\')
     * -recompose \ n by \n
     * -recompose '% 1 $ s' by ' %1$s ' and so on (2,3...)
     * This process should be done for EVERY strings.xml ,default included (for the case \')
     * TODO: The form has to explain how it works (takes data from Google and then normalize it wihtout going again to API)
     *
     * @param node
     */

    private static void nodeIteration(Node node) {
        // do something with the current node instead of System.out
        System.out.println(node.getNodeName());

        NodeList nodeList = node.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node currentNode = nodeList.item(i);
            if (currentNode.getNodeType() == Node.ELEMENT_NODE) {
                if (currentNode.getTextContent() != null) {
                    processNode(currentNode);
                } else {
                    System.out.println("Node " + currentNode.getNodeName() + " value null...");
                }
                nodeIteration(currentNode);
            }
        }
    }

    /* All stack of steps for node content is happening here*/
    private static void processNode(Node currentNode) {
        //Step 1: get the content and strip spaces added by Google
        String nodeValue = currentNode.getTextContent().trim();
        //Step 2: solve the ' issue: all apostrophes need to be replaced with \'
        //Since '\' is a special character, we need to use double \
        if (nodeValue.indexOf("'") != -1) {
            nodeValue = nodeValue.replace("'", "\\'");
            //Step 2.1: Previous step introduces a potential problem: all /' characters are replaced by //'
            nodeValue = nodeValue.replace("\\\\", "\\");
        }

        //Step 3: recompose \n. Google destroys this and leaves '\ n\ instead
        nodeValue = nodeValue.replace("\\ n", "\\n");

        //Step 4 & 5. Recompose variables. Google destroys variables like ' %1$s' leaving '% 1 $ s' instead
        //I will do just the first two variables
        nodeValue = nodeValue.replace("% 1 $ s", " %1$s");
        nodeValue = nodeValue.replace("% 2 $ s", " %2$s");
        currentNode.setTextContent(nodeValue);
    }

    /**
     * Google Cloud Translate does NOT prettyfies XML document to be translated.
     * This method solves it.
     *
     * @param document
     * @param indent
     * @return
     */
    private static String toPrettyString(Document document, int indent) {
        try {
            // Turn xml string into a document
            document.normalize();
            XPath xPath = XPathFactory.newInstance().newXPath();
            NodeList nodeList = (NodeList) xPath.evaluate("//text()[normalize-space()='']",
                    document,
                    XPathConstants.NODESET);

            for (int i = 0; i < nodeList.getLength(); ++i) {
                Node node = nodeList.item(i);
                node.getParentNode().removeChild(node);
            }

            // Setup pretty print options
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            transformerFactory.setAttribute("indent-number", indent);
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            // Return pretty print xml string
            StringWriter stringWriter = new StringWriter();
            transformer.transform(new DOMSource(document), new StreamResult(stringWriter));
            return stringWriter.toString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    //Create and generates the proper folder for translation.
    //Concept: for first value (default, english) create res/values/string.xml
    //For next values (for example, spanish (es)) create res/values-es/string.xml
    //For complex values (zh-CN ) create res/values-zh-rCN/string.xml
    // initalFolder should be res/values , or whatever
    private static String generateFolderName(String initialFolder, int x, DataPack.TransData td) {
        if (x == 0) return initialFolder + "values/";
        return initialFolder + "values" + makePostfix(td.sLanguageCode) + "/";
    }

    /**
     * Given postfix , makes the correspondent folder postfix
     * Also make many validations, in case of improper postfix
     * DOES NOT raise an error.
     * Example: ca ->ca
     * eu-ES ->eu-rES
     *
     * @param postfix
     * @return
     */
    private static String makePostfix(String postfix) {
        if (postfix.length() < 2) {
            System.out.println("ERROR: Language Code " + postfix + " has less than 2 characters");
            return "??";
        }
        if (postfix.length() == 2) {
            return "-" + postfix.toLowerCase();
        }
        /*Exception es-419
        if (postfix.length()!=5) {
            System.out.print("ERROR: Language Code " + postfix + " need to have 5 characters (xx-yy)");
            return "??-??";
        }
        */
        String[] prefixes = postfix.split("-");
        if (prefixes.length != 2) {
            System.out.print("ERROR: Language Code " + postfix + " missing '-' character");
            return "??x??";
        }
        if (prefixes[0].length() != 2) {
            System.out.print("ERROR: Language Code " + prefixes[0] + " needs to have 2 characters!(" + postfix + ")");
            return "??";
        }

        if (prefixes[1].length() > 3) {
            System.out.print("ERROR: Language Code " + prefixes[1] + " needs to have at least 3 characters!(" + postfix + ")");
            return "??";
        }

        return "-" + prefixes[0].toLowerCase() + "-r" + prefixes[1];
    }
}
