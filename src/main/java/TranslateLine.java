import java.util.ArrayList;
import java.util.List;

//Represent groups of lines being or not translated.

public class TranslateLine {

    public List<String> getLines() {
        return lines;
    }

    public void clearLines(){
        this.lines.clear();
    }

    public void addLine(String line) {
        this.lines.add(line);
    }

    public Boolean getTranslate() {
        return Translate;
    }

    public void setTranslate(Boolean translate) {
        Translate = translate;
    }

    List<String> lines;
    Boolean Translate;

    public TranslateLine() {
        lines = new ArrayList<String>();
        this.Translate = false;
    }
}