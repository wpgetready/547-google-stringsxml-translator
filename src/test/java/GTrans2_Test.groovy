import groovy.util.logging.Log
import org.junit.Assert

class GTrans2_Test extends GroovyTestCase {
    void testDoTranslateV2_normal_case_no_tags() {
        String test1 = "";
        test1 += "Line 1\n";
        test1 += "Line 2\n";
        test1 += "Line 3\n";
        GTrans2 tst = new GTrans2();
        List<TranslateLine> result = tst.getLinesToTranslate(test1)
        Log.println(result);
        //Se predice UN grupo de 3 lineas con translate true
        Assert.assertEquals("Only one group should be returned!", 1,result.size());
        Assert.assertEquals("Group size should be 3!", 3,result[0].lines.size());
        Assert.assertEquals("The group sholud be translatable!", true,result[0].translate);
    }

    void testDoTranslateV2_normal_case_all_tags() {

        String test1 = "";
        test1 += GTrans2.DNT_1 + "\n";//IMPORTANT: Every do not translate should be in their own line.
        test1 += "Line 1\n";
        test1 += "Line 2\n";
        test1 += "Line 3\n";
        test1 += GTrans2.DNT_1_CLOSE + "\n";//IMPORTANT: Every do not translate should be in their own line.
        GTrans2 tst = new GTrans2();
        List<TranslateLine> result = tst.getLinesToTranslate(test1)
        Log.println(result);
        //Se predice UN grupo de 3 lineas con translate true
        Assert.assertEquals("Only one group should be returned!", 1,result.size());
        Assert.assertEquals("Group size should be 3!", 3,result[0].lines.size());
        Assert.assertEquals("The group sholud be NOT translatable!", false,result[0].translate);
    }

    //Detect incorrect apertures.Every Do_Not_translate needs to be properly closed.
    void testDoTranslateV2_incorrect_opening_Tag() {
        String test1 = "";
        test1 += "Line 1\n";
        test1 += "Line 2\n";
        test1 += "Line 3\n";
        test1 += GTrans2.DNT_1 + "\n";//IMPORTANT: Every do not translate should be in their own line.
        test1 += "Line 4\n";
        test1 += "Line 5\n";
        GTrans2 tst = new GTrans2();
        try {
            List<TranslateLine> result = tst.getLinesToTranslate(test1)
        } catch (Exception e) {
            Log.println(e.getMessage());
            Assert.assertTrue("Ok",true);
            return;
        }
        Assert.assertTrue("This test should raise an error!", false);
    }

    //Improper closure. Every tag needs to be properly opened and closed.
    void testDoTranslateV2_incorrect_closing_Tag() {
        String test1 = "";
        test1 += "Line 1\n";
        test1 += "Line 2\n";
        test1 += "Line 3\n";
        test1 += GTrans2.DNT_1_CLOSE + "\n";//IMPORTANT: Every do not translate should be in their own line.
        test1 += "Line 4\n";
        test1 += "Line 5\n";
        GTrans2 tst = new GTrans2();
        try {
            List<TranslateLine> result = tst.getLinesToTranslate(test1)
        } catch (Exception e) {
            Log.println(e.getMessage());
            Assert.assertTrue("Ok",true);
            return;
        }
        Assert.assertTrue("This test should raise an error!", false);
    }

//Normal case 2 groups one translatable, one not.
    void testDoTranslateV2_normal_case_2Groups() {
        String test1 = "";
        test1 += "Line 1\n";
        test1 += "Line 2\n";
        test1 += "Line 3\n";
        test1 += GTrans2.DNT_1 + "\n";//IMPORTANT: Every do not translate should be in their own line.
        test1 += "Line 4\n";
        test1 += "Line 5\n";
        test1 += GTrans2.DNT_1_CLOSE + "\n";//IMPORTANT: Every do not translate should be in their own line.
        GTrans2 tst = new GTrans2();
        List<TranslateLine> result = tst.getLinesToTranslate(test1)
        Log.println(result);
        //Se predice dos grupos uno de 3 lineas (a traducir) y otro de 2 lineas (a no traducir)
        Assert.assertEquals("Two groups should be returned!", 2,result.size());
        Assert.assertEquals("First Group size should be 3!", 3,result[0].lines.size() );
        Assert.assertEquals("Second Group size should be 2!", 2,result[1].lines.size());
        Assert.assertEquals("First group should be translatable!", true,result[0].translate);
        Assert.assertEquals("Second group should be NOT translatable!", false,result[1].translate);
    }


    //Little more complex test. Three groups ,one is splitting the groups
    void testDoTranslateV2_3Groups() {
        String test1 = "";
        test1 += "Line 1\n";
        test1 += "Line 2\n";
        test1 += "Line 3\n";
        test1 += GTrans2.DNT_1 + "\n";//IMPORTANT: Every do not translate should be in their own line.
        test1 += "Line 4\n";
        test1 += "Line 5\n";
        test1 += GTrans2.DNT_1_CLOSE + "\n";//IMPORTANT: Every do not translate should be in their own line.
        test1 += "Line 6\n";
        test1 += "Line 7\n";
        test1 += "Line 8\n";
        test1 += "Line 9\n";
        GTrans2 tst = new GTrans2();
        List<TranslateLine> result = tst.getLinesToTranslate(test1)
        Log.println(result);
        //Se predice dos grupos uno de 3 lineas (a traducir) y otro de 2 lineas (a no traducir)
        Assert.assertEquals("3 groups should be returned!", 3,result.size());
        Assert.assertEquals("First Group size should be size 3!", 3,result[0].lines.size() );
        Assert.assertEquals("Second Group size should be size 2!", 2,result[1].lines.size());
        Assert.assertEquals("Third Group size should be size 4!", 4,result[2].lines.size());
        Assert.assertEquals("First group should be translatable!", true,result[0].translate);
        Assert.assertEquals("Second group should be NOT translatable!", false,result[1].translate);
        Assert.assertEquals("Third group should be translatable!", true,result[2].translate);
    }

    //More complex test with empty blocks.Empty groups should be completely ignored and not even counted.
    void testDoTranslateV2_Ignore_empty_groups() {
        String test1 = "";
        test1 += "Line 1\n";
        test1 += "Line 2\n";
        test1 += "Line 3\n";
        test1 += GTrans2.DNT_1 + "\n";//IMPORTANT: Every do not translate should be in their own line.
        test1 += "Line 4\n";
        test1 += "Line 5\n";
        test1 += GTrans2.DNT_1_CLOSE + "\n";//IMPORTANT: Every do not translate should be in their own line.
        //This is an empty block and should be ignored completely
        test1 += GTrans2.DNT_1 + "\n";//IMPORTANT: Every do not translate should be in their own line.
        test1 += GTrans2.DNT_1_CLOSE + "\n";//IMPORTANT: Every do not translate should be in their own line.

        test1 += "Line 6\n";
        test1 += "Line 7\n";
        test1 += "Line 8\n";
        test1 += "Line 9\n";
        //This is an empty block and should be ignored completely
        test1 += GTrans2.DNT_1 + "\n";//IMPORTANT: Every do not translate should be in their own line.
        test1 += GTrans2.DNT_1_CLOSE + "\n";//IMPORTANT: Every do not translate should be in their own line.

        GTrans2 tst = new GTrans2();
        List<TranslateLine> result = tst.getLinesToTranslate(test1)
        Log.println(result);
        //Se predice dos grupos uno de 3 lineas (a traducir) y otro de 2 lineas (a no traducir)
        Assert.assertEquals("3 groups should be returned!", 3,result.size());
        Assert.assertEquals("First Group size should be size 3!", 3,result[0].lines.size() );
        Assert.assertEquals("Second Group size should be size 2!", 2,result[1].lines.size());
        Assert.assertEquals("Third Group size should be size 4!", 4,result[2].lines.size());
        Assert.assertEquals("First group should be translatable!", true,result[0].translate);
        Assert.assertEquals("Second group should be NOT translatable!", false,result[1].translate);
        Assert.assertEquals("Third group should be translatable!", true,result[2].translate);
    }

    //Nested blocks are NOT allowed. Repeated opening tags case
    void testDoTranslateV2_disallow_nested_blocks() {
        String test1 = "";
        test1 += "Line 1\n";
        test1 += "Line 2\n";
        test1 += "Line 3\n";
        test1 += GTrans2.DNT_1 + "\n";//IMPORTANT: Every do not translate should be in their own line.
        test1 += "Line 4\n";
        //This is an empty block and should be ignored completely
        test1 += GTrans2.DNT_1 + "\n";//IMPORTANT: Every do not translate should be in their own line.
        test1 += GTrans2.DNT_1_CLOSE + "\n";//IMPORTANT: Every do not translate should be in their own line.

        test1 += "Line 5\n";
        test1 += GTrans2.DNT_1_CLOSE + "\n";//IMPORTANT: Every do not translate should be in their own line.

        test1 += "Line 6\n";
        test1 += "Line 7\n";
        test1 += "Line 8\n";
        test1 += "Line 9\n";
        //This is an empty block and should be ignored completely
        test1 += GTrans2.DNT_1 + "\n";//IMPORTANT: Every do not translate should be in their own line.
        test1 += GTrans2.DNT_1_CLOSE + "\n";//IMPORTANT: Every do not translate should be in their own line.

        try {
            GTrans2 tst = new GTrans2()
            List<TranslateLine> result = tst.getLinesToTranslate(test1)
        } catch (Exception e) {
            Log.println(e.getMessage());
            Assert.assertTrue("Ok",true);
            return;
        }
        Assert.assertTrue("This test should raise an error!", false);
    }

    //Version blocks needs to be properly closed. Every open tag should have a proper closed one.
    void testDoTranslateV2_improper_closing_blocks() {
        String test1 = "";
        test1 += "Line 1\n";
        test1 += "Line 2\n";
        test1 += "Line 3\n";
        test1 += GTrans2.DNT_1 + "\n";//IMPORTANT: Every do not translate should be in their own line.
        test1 += "Line 4\n";
        //This is an empty block and should be ignored completely
        //test1 += GTrans2.DNT_1 + "\n";//IMPORTANT: Every do not translate should be in their own line.
        test1 += GTrans2.DNT_1_CLOSE + "\n";//IMPORTANT: Every do not translate should be in their own line.

        test1 += "Line 5\n";
        test1 += GTrans2.DNT_1_CLOSE + "\n";//IMPORTANT: Every do not translate should be in their own line.

        test1 += "Line 6\n";
        test1 += "Line 7\n";
        test1 += "Line 8\n";
        test1 += "Line 9\n";
        //This is an empty block and should be ignored completely
        test1 += GTrans2.DNT_1 + "\n";//IMPORTANT: Every do not translate should be in their own line.
        test1 += GTrans2.DNT_1_CLOSE + "\n";//IMPORTANT: Every do not translate should be in their own line.

        try {
            GTrans2 tst = new GTrans2()
            List<TranslateLine> result = tst.getLinesToTranslate(test1)
        } catch (Exception e) {
            Log.println(e.getMessage());
            Assert.assertTrue("Ok",true);
            return;
        }
        Assert.assertTrue("This test should raise an error!", false);
    }
}
